# graphql-springboot2.2.6.release-java1.8-gradle-h2-template-1.0

graphql-springboot2.2.6.release-java1.8-gradle-h2-template-1.0

#### Only GraphQL UI testing - localhost:8010/graphiql or localhost:8010/gui
Note: Change the host url based on your ip ->  host-ip:8010/graphiql or host-ip:8010/gui

for k8s clusters, no need to provide PORT

1. Query to get all users with field id, email and username 
```
   {
     users {
       id
       email
       username
     }
   }
```
2. Query to create new User
```
mutation {
  saveUser(user: {username: "user123", email: "user123@xyz.com"}) {
    id
  }
}
```

### Please check below examples to run more queries 

#### Rest Endpoint with GraphQL - localhost:8010/rest-graphql/usersCrud
#### With this single POST endpoint, you will be able to run all CRUD Operations as listed below
Note: Change the host url based on your ip ->  host-ip:8010/graphiql or host-ip:8010/gui

for k8s clusters, no need to provide PORT
 
1. POST request to get all users with field id, email and username 
```
   {
     users {
       id
       email
       username
     }
   }
```
2. POST request to get all users with field only id and email
```
{
  users {
    id
    email
  }
}
```
3. POST request to get single user with specific Id provided
```
{
  user(id: 1) {
    id
    email
  }
}
```
4. POST request to create new User
```
mutation {
  saveUser(user: {username: "user123", email: "user123@xyz.com"}) {
    id
  }
}
```
5. POST request to delete the User with Id
```
mutation {
  deleteUser(id :2)
}
```
6. POST request to update the User with new details
```
mutation {
  updateUser(id: 1, username: "demouser123", email: "demouser123@xyz.com") {
    id
  }
}
```
