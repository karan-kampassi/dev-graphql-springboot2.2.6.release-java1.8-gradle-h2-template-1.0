package com.altimetrik.demo.service;

import com.altimetrik.demo.entity.User;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;

import java.util.List;

public interface UserService {
    List<User> getAll();

    User getById(Long id);

    User create(@GraphQLArgument(name = "user") User user);

    User update(Long id, String username, String email);

    boolean delete(Long id);
}
